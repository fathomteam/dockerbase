# Builds a Docker image with all the necessary compiler and
# build essentials needed to test SIGMA packages and
# computational toolchains.
#
# Authors:
# Vijay Mahadevan <vijay.m@gmail.com>
FROM ubuntu:22.04
MAINTAINER Vijay Mahadevan <vijay.m@gmail.com>

ENV HOME /home/sigma

# Install add-apt-repository
RUN apt-get -qq update \
    && apt-get -qqy install software-properties-common \
    wget pkg-config git libopenblas-dev liblapack-dev \
    make cmake autoconf automake libtool python3 python3-pip \
    clang gcc g++ gfortran \
    libhdf5-dev libhdf5-mpi-dev  libnetcdf-mpi-dev libnetcdf-pnetcdf-dev \
    libeigen3-dev libmetis-dev doxygen \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pip3 install setuptools cython scipy nose packaging

#RUN groupadd -r sigmauser -g 433 && useradd -u 431 -r -g sigmauser -d /home/sigma -s /usr/sbin/nologin -c "Docker image - SIGMA user" sigmauser \
#    && chown -R sigmauser:sigmauser /home/sigma && echo "sigmauser:docker" | chpasswd \
#    && usermod -a -G sudo sigmauser

# This makes sure we launch with ENTRYPOINT /bin/bash into the home directory
ADD WELCOMEMSG /home/sigma/.WELCOMEMSG
ADD .bashrc_SIGMA /home/sigma/.bashrc
ADD bootstrapSIGMA /home/sigma/bin/bootstrapSIGMA

#ENTRYPOINT ["/sbin/setuser","sigmauser","/bin/bash","-c"]
#CMD ["/sbin/my_init"]

